angular.module('timi.controllers', ['ui.bootstrap','ngStorage','toastr'])
.config(function($httpProvider,$compileProvider) {
    $compileProvider.preAssignBindingsEnabled(true);
     $httpProvider.interceptors.push(function($q,$location,$localStorage,toastr) {
        return {
          responseError: function(rejection) {
                if(rejection.status <= 0) {
                    delete $localStorage.login;
                    delete $localStorage.userData;
                    $location.path("/app/guest/login");
                    toastr.error("Unable to connect to server. Please contact the administrator","Network");
                    //return;
                }
                return $q.reject(rejection);
            }
        };
    });
})
.controller('appController', function ($scope, $rootScope,$localStorage,$location) {

    $rootScope.baseURL = 'http://104.236.58.33:3009';

})

.controller('headerController', function ($scope,$http, $rootScope,$localStorage,$state,toastr) {

    $scope.logout=function(){

         toastr.success('Logged out successfully','Success');
        $state.go("app.guest.login");
         delete $localStorage.login;
        delete $localStorage.userData;
        $rootScope.loggedin = false;
        console.log($localStorage);
    }
})

.controller('listController', function ($scope,$http, $rootScope,$localStorage,$filter,$state) {
    $rootScope.page  = "app.member.list";
    $rootScope.$emit('page-change','app.member.list');
    if(!$localStorage.login)
        $state.go("app.guest.login");
    if(!$localStorage.hasOwnProperty('userData'))
        $state.go("app.guest.login");
    $http.defaults.headers.common['Authorization'] = 'JWT ' + $localStorage.userData.token;
    $rootScope.loggedin = true;
    $scope.currentPage =1;
    $scope.q="";
    var url = "/patients/list?page=1&search=";
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {

                if (response.data.status_code=='0')
                {
                    $scope.users = response.data.result;
                    $scope.totalPages = response.data.pages;
                    $scope.currentPage = parseInt(response.data.page);
                    $scope.total = response.data.datacount;
                }
                else{
                    alert("error in pulling the data");
                }
        });

    $scope.search =function(){
        url = "/patients/list?page=1&search="+$scope.q;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {

                if (response.data.status_code=='0')
                {
                    $scope.users = response.data.result;
                    $scope.totalPages = response.data.pages;
                    $scope.currentPage = parseInt(response.data.page);
                    $scope.total = response.data.datacount;
                }
                else{
                    alert("error in pulling the data");
                }
        });
    }

    $scope.next=function($event){
        $event.preventDefault();
        if($scope.currentPage<$scope.totalPages){
            var next = $scope.currentPage+1;
            url = "/patients/list?page="+next+"&search="+$scope.q;
                var sec = $http.get($rootScope.baseURL + url)
                .then(function(response) {

                    if (response.data.status_code=='0')
                    {
                        $scope.users = response.data.result;
                        $scope.totalPages = response.data.pages;
                        $scope.currentPage = parseInt(response.data.page);
                        $scope.total = response.data.datacount;
                    }
                    else{
                        alert("error in pulling the data");
                    }
            });
        }
    }

    $scope.addNew=function(){
        $state.go('app.member.content',{id:0});
    }

    $scope.prev=function($event){
        $event.preventDefault();
        if($scope.currentPage>1){
            var prev = $scope.currentPage -1;
            url = "/patients/list?page="+prev+"&search="+$scope.q;
                var sec = $http.get($rootScope.baseURL + url)
                .then(function(response) {

                    if (response.data.status_code=='0')
                    {
                        $scope.users = response.data.result;
                        $scope.totalPages = response.data.pages;
                        $scope.currentPage = parseInt(response.data.page);
                        $scope.total = response.data.datacount;
                    }
                    else{
                        alert("error in pulling the data");
                    }
            });
        }
    }

    $scope.load=function(id){
        $state.go("app.member.content",{id:id});
    }

    $scope.keydown=function(){}
})
.controller('sidenavController', function ($scope,$localStorage, $rootScope,$localStorage) {
    $scope.name = $localStorage.userData.name+ " "+$localStorage.userData.lname;
    $scope.title = $localStorage.userData.title;
    $rootScope.$on('page-change',function(){
        $scope.page = $rootScope.page;
    });
})
.controller('loginController', function ($scope, $rootScope,$localStorage,$state,$http,toastr) {
    $rootScope.loggedin = false;
    $scope.user ={};
    if($localStorage.login){
        $rootScope.loggedin = true;
        $state.go("app.member.list");
    }
    $scope.signup=function(){
        $state.go("app.guest.signup");
    }
    $scope.login =function(){
        var url = "/login/authenticate";
        if($scope.user.hasOwnProperty('username')&&$scope.user.hasOwnProperty('password'))
        {
                var sec = $http.post($rootScope.baseURL + url,$scope.user)
                .then(function(response) {

                    if (response.data.status_code=='401')
                    {
                        toastr.clear();
                        toastr.error("Please enter valid username and password","Invalid login");
                    }
                    else if(response.data.status_code=='200')
                    {
                        console.log(response.data);
                        $localStorage.userData = response.data.result;
                        $localStorage.login=true;
                        $state.go("app.member.list");
                    }
            });
        }
        else
        {
            toastr.clear();
            toastr.error("Please enter username and password","Error");
        }
    }
})
.controller('signupController', function ($scope,$http,$state, $rootScope,$localStorage,toastr) {
    $rootScope.loggedin =false;
    var url = "/users/signup";
    $scope.user = {};
    $scope.login=function(){
        $state.go("app.guest.login");
    }
    $scope.signup=function(){
        if($scope.user.hasOwnProperty('username')&&$scope.user.hasOwnProperty('password')
                &&$scope.user.hasOwnProperty('cpassword')&&$scope.user.hasOwnProperty('name'))
        {
            if($scope.user.cpassword==$scope.user.password){
                var sec = $http.post($rootScope.baseURL + url,$scope.user)
                    .then(function(response) {

                        if (response.data.status_code=='1')
                        {
                            toastr.clear();
                            toastr.error(response.data.message,"Error");
                        }
                        else if(response.data.status_code=='0')
                        {
                            toastr.clear();
                            toastr.success("User account created. Please login to proceeed","Success");
                            $state.go("app.guest.login");
                        }
                });
            }else{
                toastr.clear();
                            toastr.error("Password and confirm password doest not match","Error");
            }
        }
        else
        {
            toastr.clear();
            toastr.error("Please enter all details","Error");
        }
    }

})

.controller('contentController', function ($scope, $rootScope, $http,$filter,$localStorage,$state,toastr,$location) {
    $rootScope.page  = "app.member.content";
    $rootScope.$emit('page-change','app.member.content');
    if(!$localStorage.login)
        $state.go("app.guest.login");
    if(!$localStorage.hasOwnProperty('userData'))
        $state.go("app.guest.login");
$scope.doctor_name = $localStorage.userData.name+ " "+$localStorage.userData.lname;
    toastr.options = {
  "closeButton": true,
  "newestOnTop": true,
  "preventDuplicates": true,
  "showDuration": "300",
  "hideDuration": "800",
  "timeOut": "1000",
  "showEasing": "linear",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

    $http.defaults.headers.common['Authorization'] = 'JWT ' + $localStorage.userData.token;
    $rootScope.loggedin = true;
    $scope.patients ={};
    $scope.demographics ={};
    $scope.visit_information ={};
    $scope.chief_complaint ={};
    $scope.history_of_present_illness ={};
    $scope.review_of_systems ={};
    $scope.allergies ={};
    $scope.medications={};
    $scope.problem_list={};
    $scope.histories ={};
    $scope.health_maintenance={};
    $scope.physical_examination={};
    console.log($state);
    $scope.id=$state.params.id;
    $scope.tab=1;
    $scope.visit_information.service_time =new Date();
    $scope.visit_information.date_of_service=$filter('date')(new Date(), 'MM-dd-yyyy') ;
    if($scope.id>0){
            var url = "/patients/get/demographics?id="+$scope.id;

            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.patients = response.data.result.patients;
                    var dateParts = $scope.patients.dob.split("-");
                    $scope.patients.dob = dateParts[1]+"-"+dateParts[2]+"-"+dateParts[0];
                    $scope.demographics = response.data.result.demographics;
                }
                else{
                    $state.go("app.member.list");
                    toastr.error("Patient records not available","");
                }
            });
            url = "/patients/get/visitinfomation?id="+$scope.id;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    if(response.data.result.length>0){
                        $scope.visit_information = response.data.result[0];
                        $scope.chief_complaint.complaint = response.data.result[0].complaint;
                        $scope.history_of_present_illness.history = response.data.result[0].history;
                        $scope.visit_information.date_of_service =$filter('date')(new Date( $scope.visit_information.date_of_service), 'MM-dd-yyyy') ;
                        $scope.visit_information.service_time = new Date($scope.visit_information.date_of_service+" "+$scope.visit_information.service_time);
                        //document.getElementById("service_time").value=$scope.visit_information.date_of_service;
                    }
                }
            });
            url = "/patients/get/review?id="+$scope.id;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.review_of_systems = response.data.result;
                }
            });

            url = "/patients/get/health?id="+$scope.id;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.health_maintenance = response.data.result;
                }
            });

            url = "/patients/get/physical_examination?id="+$scope.id;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.physical_examination = response.data.result;
                    $scope.physical_examination.vital_signs =$filter('date')(new Date( $scope.physical_examination.vital_signs), 'MM-dd-yyyy') ;
                    $scope.physical_examination.vital_time = new Date($scope.physical_examination.vital_signs+" "+$scope.physical_examination.vital_time);
                        
                    //document.getElementById("vital_time").value=$scope.physical_examination.vital_time;
                }
            });

            url = "/patients/get/healthstatus?id="+$scope.id;
            var sec = $http.get($rootScope.baseURL + url)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    if(response.data.result.length>0){
                        $scope.histories = response.data.result[0];
                        $scope.allergies.allergies = response.data.result[0].allergies;
                        $scope.medications.medications = response.data.result[0].medications;
                        $scope.problem_list.problem_list = response.data.result[0].problem_list;
                    }
                }
            });
        }


    $scope.constitutional = ["Negative  ","  Negative except HPI  ","  Fever   ","  Chills   ","  Sweats  ","  Weakness  ","  Fatigue  ","  Decreased activity  ","   OTHER"];
    $scope.eye = ["Negative  ","  Recent visual problem  ","   Negative except HPI  ","  Icterus  ","  Discharge+  ","   Blurring   ","  Double vision   ","  Visual disturbances   ","  OTHER"];
    $scope.ear_nose_mouth_throat_enmt =["Negative  ","  Negative except HPI    ","  Decreased hearing+  ","  Ear pain+   ","   Nasal congestion  ","  Sore throat  "," OTHER"];
    $scope.forth =["Negative "," Negative except HPI  "," SOB  ","  Cough  ","  Sputum production  ","  Hemoptysis  ","  Wheezing  "," Cyanosis  ","  Apnea  ","  OTHER"];
    $scope.fifth=["Negative  ","  Negative except HPI  ","  Chest pain+  ","  Palpitations  ","  Bradycardia   ","  Tachycardia   ","   Peripheral edema  ","  Syncope  ","  OTHER"];
    $scope.sixth=["Negative  ","  Negative except HPI "," Nausea "," Vomiting  "," Diarrhea  "," Constipation  ","  Heartburn  ","  Abdominal pain+   "," Hematemesis  ","  Other"];
    $scope.seventh=["Negative  ","  Negative except HPI  "," Dysuria  ","  Hematuria "," Change in urine stream  ","   Urethral discharge  ","  Lesions   ","  OTHER"];
    $scope.s8=["Negative  ","  Negative except HPI  ","   Bruising tendency "," Bleeding tendency  ","  Swollen lymph glands  ","   OTHER"];
    $scope.s9=["Negative  ","  Negative except HPI  ","   Excessive thirst  ","  Polyuria  ","  Cold intolerance  ","   Excessive hunger  ","   OTHER"];
    $scope.s10=["Negative  ","  Negative except HPI  ","  Immunocompromised  ","  Recurrent fevers  ","   Recurrent infections  ","  Malaise  ","  OTHER"];
    $scope.s11=["Negative  ","  Negative except HPI  ","  Back pain+   ","  Neck pain  ","  Joint pain ","  Muscle pain  ","  Claudication  ","  Decreased ROM  ","  Trauma  ","  OTHER"];
    $scope.s12=["Negative  ","  Negative except HPI  ","  Rash  ","  Pruritus ","  Abrasions  "," Breakdown  ","  Burns  ","  Dryness ","  Petechiae "," Skin lesion ","  Hypertrophic scar  ","  Keloid  ","   No other significant skin complaints  ","  OTHER"];
    $scope.s13=["Negative  ","  Negative except HPI  ","  Alert & oriented X 4  ","  Abnormal balance  ","  Confusion  ","  Numbness  ","  Tingling   "," Headache  "," OTHER"];
    $scope.s14=["Negative  ","  Negative except HPI  ","  Anxiety   ","  Depression   ","   Mania   "," Suicidal  ","  Delusional   ","  Hallucinations   ","   OTHER"];




    $scope.h={};
    $scope.h[1]={};
    $scope.h[2]={};
    $scope.h[3]={};
    $scope.h[4]={};
    $scope.h[5]={};
    $scope.h[6]={};
    $scope.h[7]={};
    $scope.h[8]={};
    $scope.h[9]={};
    $scope.h[10]={};
    $scope.h[11]={};
    $scope.h[12]={};
    $scope.h[13]={};
    $scope.h[14]={};


$scope.h[1]['v']=["Include general physical exam from PowerChart Touch "," Alert and oriented  ","  No acute discress  ","  Mild distress  ","  Moderate distress  ","  Severe distress  ","  Ramsay sedation scale+  ","  OTHER"];
$scope.h[2]['v']=["PERRL  ","  EOMI  "," Normal conjunctiva  "," Vision unchanged  ","  OTHER"];
$scope.h[3]['v']=["Normocephalic  ","TMs clear  ","  Normal hearing  ","  Moist oral mucosa  ","  No pharyngeal erythema  "," No sinus tenderness  ","  OTHER"];
$scope.h[4]['v']=["Supple  ","  Non-tender ","No carotid bruit  ","  No jugular venous distention  ","  No lymphadenopathy ","  No thyromegaly  "," OTHER"];
$scope.h[5]['v']=["Lungs CTA ","   Non-labored respirations  ","  BS equal  ","  Symmetrical expansion  ","  No chest wall tenderness  ","  OTHER"];
$scope.h[6]['v']=["Normal rate  ","  Regular rhythm  ","  No murmur  ","  No gallop   ","  Good pulses equal in all extremities  ","  Normal peripheral perfusion   ","   No edema  ","  OTHER"];
$scope.h[7]['v']=["Soft  ","  Non-tender  ","  Non-distended  ","  Normal bowel sounds  ","  No lesions   ","   OTHER"];
$scope.h[8]['v']=["No CVA tenderness   ","  No inguinal tenderness  ","  No urethral discharge   ","  No lesions  ","  OTHER"];
$scope.h[9]['v']=["No lymphadenopathy neck, axilla, groin ","  OTHER"];
$scope.h[10]['v']=["Normal ROM  ","  Normal strength  ","   No tenderness  ","   No swelling   ","  No deformity  ","   Normal gait   ","   OTHER"];
$scope.h[11]['v']=["Warm  ","  Dry   ","  Pink   ","   Cyanotic  ","  Intact  ","  Moist  ","  No pallor  ","  No rash  ","  OTHER"];
$scope.h[12]['v']=["Alert  ","   Oriented  ","  Normal sensory  ","   Normal motor   ","  No focal deficits  ","  CN II-XII intact   ","  Normal DTR’s  ","  OTHER"];
$scope.h[13]['v']=["Oriented  ","  Speech clear and coherent  ","  Functional cognition intact   ","  OTHER"];
$scope.h[14]['v']=["Cooperative   ","   Appropriate mood & affect  ","  Normal judgment  ","  Non-suicidal  ","  OTHER"];

$scope.h[1]['n']="General";
$scope.h[2]['n']="Eye";
$scope.h[3]['n']="HENT";
$scope.h[4]['n']="Neck";
$scope.h[5]['n']="Respiratory";
$scope.h[6]['n']="Cardiovascular";
$scope.h[7]['n']="Gastrointestinal";
$scope.h[8]['n']="Genitourinary";
$scope.h[9]['n']="Lymphatics";
$scope.h[10]['n']="Musculoskeletal";
$scope.h[11]['n']="Integumentary";
$scope.h[12]['n']="Neurologic";
$scope.h[13]['n']="Cognition and Speech";
$scope.h[14]['n']="Psychiatric";

$scope.h[1]['m']="general";
$scope.h[2]['m']="eye";
$scope.h[3]['m']="hent";
$scope.h[4]['m']="neck";
$scope.h[5]['m']="respiratory";
$scope.h[6]['m']="cardiovascular";
$scope.h[7]['m']="gastrointestinal";
$scope.h[8]['m']="genitourinary";
$scope.h[9]['m']="lymphatics";
$scope.h[10]['m']="musculoskeletal";
$scope.h[11]['m']="integumentary";
$scope.h[12]['m']="neurologic";
$scope.h[13]['m']="cognition_and_speech";
$scope.h[14]['m']="psychiatric";




    $scope.trim =function(value){
        return value.trim();
    }

    $scope.saveTab1 = function(){
        if($scope.id>0)
            $scope.patients.idpatients = $scope.id;
        var url = "/patients/update/patients";
            var sec = $http.post($rootScope.baseURL + url,$scope.patients)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    if($scope.id==0){
                        $scope.id = response.data.result.idpatients;
                        $scope.patients.idpatients= $scope.id;
                    }
                    $scope.demographics.idpatients = $scope.id;
                    url="/patients/update/demographics";

                   var sec = $http.post($rootScope.baseURL + url,$scope.demographics)
                    .then(function(response) {
                        if (response.data.status_code == '0')
                        {
                            $scope.tab=2;
                            toastr.success("Demographics updated successfully","Success",toastr.options);

                        }
                        else if(response.data.status_code == '9')
                        {
                            toastr.error("Unable to update demographics","Error",toastr.options);

                        }
                    }, function(){});
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update demographics","Error",toastr.options);

                }
            }, function(){});
    }

    $scope.saveTab2 = function(){

        var url = "/patients/update/visitinformation";
            var data ={};
            data.visit_information = $scope.visit_information;
            data.visit_information.service_time = $filter('date')(new Date( $scope.visit_information.service_time), 'HH:mm:ss') ;
            data.chief_complaint = $scope.chief_complaint;
            data.history_of_present_illness = $scope.history_of_present_illness;

            data.visit_information.idpatients = $scope.id;
            data.visit_information.author = $scope.doctor_name;
            //data.visit_information.date_of_service  = data.visit_information.date_of_service+" "+data.visit_information.time;
            data.chief_complaint.idpatients = $scope.id;
            data.history_of_present_illness.idpatients = $scope.id;

            var sec = $http.post($rootScope.baseURL + url,data)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.tab=3;
                    toastr.success("Visit information updated successfully","Success",toastr.options);
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update Visit Information","Error",toastr.options);

                }
            }, function(){});
    }

    $scope.saveTab3 = function(){
        $scope.review_of_systems.idpatients = $scope.id;
        var url = "/patients//update/review";
            var sec = $http.post($rootScope.baseURL + url,$scope.review_of_systems)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.tab =4;
                    toastr.success("Review of systems updated successfully","Success",toastr.options);
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update Review of systems","Error",toastr.options);

                }
            }, function(){});
    }

    $scope.saveTab4 = function(){

        var url = "/patients/update/healthstatus";
            var data ={};
            data.allergies = $scope.allergies;
            data.medications = $scope.medications;
            data.problemlist = {problem_list:""};
            data.histories = $scope.histories;

            data.allergies.idpatients = $scope.id;
            data.medications.idpatients = $scope.id;
            data.problemlist.idpatients = $scope.id;
            data.histories.idpatients = $scope.id;
            var sec = $http.post($rootScope.baseURL + url,data)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    toastr.success("Health status updated successfully","Success",toastr.options);
                    $scope.tab=5;
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update Health status","Error",toastr.options);

                }
            }, function(){});
    }

    $scope.saveTab5 = function(){

        var url = "/patients/update/physical_examination";
            var data ={};
            data.physical_examination = $scope.physical_examination;
            data.physical_examination.idpatients=$scope.id;
            data.physical_examination.vital_time = $filter('date')(new Date( $scope.physical_examination.vital_time), 'HH:mm:ss') ;
            
            var sec = $http.post($rootScope.baseURL + url,data)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    $scope.tab=6;
                    toastr.success("Physical examination updated successfully","Success",toastr.options);
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update Physical information","Error",toastr.options);

                }
            }, function(){});
    }


    $scope.saveTab6 = function(){

        var url = "/patients/update/health_maintenance";

            $scope.health_maintenance.idpatients=$scope.id;

            var sec = $http.post($rootScope.baseURL + url,$scope.health_maintenance)
            .then(function(response) {
                if (response.data.status_code == '0')
                {
                    
                    toastr.success("Patient record updated successfully","Success",toastr.options);
                    try{
                        $state.go("app.memeber.list");
                    }
                    catch($e)
                    {
                        $location.path("/app/member/list");
                    }
                }
                else if(response.data.status_code == '9')
                {
                    toastr.error("Unable to update Health Maintenance","Error",toastr.options);

                }
            }, function(){});
    }
})



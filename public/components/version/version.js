'use strict';

angular.module('cds.version', [
  'cds.version.interpolate-filter',
  'cds.version.version-directive'
])

.value('version', '0.1');

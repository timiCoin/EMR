'use strict';

// Declare app level module which depends on views, and components
angular.module('timi', [
  'ui.router',
  'timi.controllers',
  'ui.bootstrap'
]).
config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {

  $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        views: {
            content: {
                template: '<div ui-view ></div>',
                controller: 'appController'
            },
            header: {
                templateUrl: 'views/commons/header.html',
                controller: 'headerController'
            },
            sidenav: {
                templateUrl: 'views/commons/sidenav.html',
                controller: 'sidenavController'
            }
        }
      })
      .state('app.member', {
        url: '/member',
        abstract: true,
          template: '<div ui-view></div>'
      })
      .state('app.member.content', {
        url: '/content/:id',
        params:{id:"0"},
          templateUrl: 'views/member/content.html',
          controller: 'contentController'
      })
      .state('app.member.list', {
        url: '/list',
          templateUrl: 'views/member/list.html',
          controller: 'listController'
      }).state('app.guest', {
          url: '/guest',
          abstract: true,
          templateUrl: 'views/guest/main.html'
      })
      .state('app.guest.signup', {
          url: '/signup',
          templateUrl: 'views/guest/signup.html',
          controller:'signupController'
      })

      .state('app.guest.login', {
        url: '/login',
        templateUrl: 'views/guest/login.html',
        controller:'loginController'
    })

  $urlRouterProvider.otherwise('/app/guest/login');
}]);

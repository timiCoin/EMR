var models = require('../models');
var express = require('express');
var router = express.Router();
var db = require('../models/dbconnection');
var speakeasy = require("speakeasy");
var QRCode = require('qrcode');
var auth = require("../models/authstrategy")();


router.get('/list',auth.authenticate(),function(req, res, next) {

    let limit = 10;  
    let offset = 0;
    let search = req.query.search;
    let page = req.query.page?req.query.page:0;  
    
    var sequelize = models.sequelize;
    
    sequelize.query("SELECT count(*) as co FROM patients  left join visit_information on patients.idpatients=visit_information.idpatients \n\
                    left join demographics d on d.idpatients = patients.idpatients  WHERE   (fname like'%"+search+"%' or lname like'%"+search+"%')",
        { type: sequelize.QueryTypes.SELECT }
      ).then(data => {
          let pages = Math.ceil(data[0].co / limit);
           offset = limit * (page - 1);
           sequelize.query("SELECT patients.*, visit_information.date_of_service,d.contact FROM patients  left join visit_information on patients.idpatients=visit_information.idpatients \n\
     left join demographics d on d.idpatients = patients.idpatients WHERE (fname like '%"+search+"%' or lname like'%"+search+"%') limit "+offset+","+limit,
                { type: sequelize.QueryTypes.SELECT }
              ).then(users => {
                 res.json({'status_code': '0',result:users,datacount: data[0].co, pages: pages,page:page});
              }).error(err=>{
                  res.json( { status_code: '9',message:"An error occured" });
              });
      }).error(err=>{
          res.json( { status_code: '9',message:"An error occured" });
      });
});



router.get('/get/demographics',auth.authenticate(),function(req, res, next) {
  var patient = models.Patient;
  var response = {};
    var value = req.query.id;
    patient.findOne({ where: {idpatients:value } }).then(p => {
        if(p){
        response.patients = p;
        models.Demographics.findOne({ where: {idpatients:value } }).then(d => {
            if(d){
                response.demographics = d;
            }else{
                response.demographics = {};
            }
            res.json( { status_code: '0', result:response });
        });
    }
    else{
            res.json( { status_code: '1', result:{} });
        }
    });
});

router.get('/get/review',auth.authenticate(),function(req, res, next) {
  var patient = models.ReviewSystem;
  var response = {};
    var value = req.query.id;
    patient.findOne({ where: {idpatients:value } }).then(p => {
        if(p){
            response = p;
            res.json( { status_code: '0', result:response });
        }
        else{
            res.json( { status_code: '1', result:response });
        }
    });
});

router.get('/get/physical_examination',auth.authenticate(),function(req, res, next) {
  var patient = models.PhysicalExam;
  var response = {};
    var value = req.query.id;
    patient.findOne({ where: {idpatients:value } }).then(p => {
        if(p){
        response = p;
            res.json( { status_code: '0', result:response });
        }
        else{
            res.json( { status_code: '1', result:[] });
        }
    });
});

router.get('/get/health',auth.authenticate(),function(req, res, next) {
  var patient = models.HealthMaintenance;
  var response = {};
    var value = req.query.id;
    patient.findOne({ where: {idpatients:value } }).then(p => {
        if(p){
            response = p;
            res.json( { status_code: '0', result:response });
        }
        else{
            res.json( { status_code: '1', result:[] });
        }
    });
});

router.get('/get/visitinfomation',auth.authenticate(),function(req, res, next) {
  var sequelize = models.sequelize;
    
    
    sequelize.query('SELECT * FROM chief_complaint c, visit_information v,history_of_present_illness h WHERE c.idpatients = v.idpatients and c.idpatients = h.idpatients and c.idpatients='+req.query.id,
        { type: sequelize.QueryTypes.SELECT }
      ).then(patients => {
        res.json( { status_code: '0', result:patients});
      }).error(err=>{
          res.json( { status_code: '9',message:"An error occured" });
      });
});

router.get('/get/healthstatus',auth.authenticate(),function(req, res, next) {
  var sequelize = models.sequelize;
            
    sequelize.query('SELECT * FROM histories h, allergies a, medications m,problem_list p WHERE h.idpatients = a.idpatients and a.idpatients = m.idpatients and a.idpatients = p.idpatients and a.idpatients='+req.query.id,
        { type: sequelize.QueryTypes.SELECT }
      ).then(patients => {
        res.json( { status_code: '0', result:patients});
      }).error(err=>{
          res.json( { status_code: '9',message:"An error occured" });
      });
});

router.get('/get/sql',auth.authenticate(),function(req, res, next) {
    var sequelize = models.sequelize;
            
    sequelize.query('SELECT * FROM patients, demographics WHERE patients.idpatients = demographics.idpatients and patients.idpatients='+req.query.id,
        { type: sequelize.QueryTypes.SELECT }
      ).then(patients => {
        res.json( { status_code: '0', result:JSON.stringify(patients) });
      }).error(err=>{
          res.json( { status_code: '9',message:"An error occured" });
      });
  
});


router.post('/update/patients',auth.authenticate(), function(req, res, next) {
    var patient = models.Patient;
    var values = req.body;
    var username = req.body.fname+req.body.lname;
        username = username.replace(/\s+/g, '-').toLowerCase();
    var objDemo = patient.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            patient.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        res.json( { status_code: '0',message:"Patients updated",
                                        });
                                    });
                        }
                        else
                        {
                            patient.create(values).then((result)=>{
                                
                                
                                models.sequelize.query("update patients set username ='"+username+"',password = SHA2('12345',256)",
                                null, { raw: true, type: 'INSERT' }
                                        ).then(patients => {
                                          //res.json( { status_code: '0', message:"User created successfully"});
                                        }).error(err=>{
                                           // res.json( { status_code: '1',message:"An error occured" });
                                        });
                                try{
                                    models.Demographics.create({idpatients:result.idpatients}).then({});
                                    models.Allergies.create({idpatients:result.idpatients}).then({});
                                    models.HealthMaintenance.create({idpatients:result.idpatients}).then({});
                                    models.History.create({idpatients:result.idpatients}).then({});
                                    models.ChiefComplain.create({idpatients:result.idpatients}).then({});
                                    models.ReviewSystem.create({idpatients:result.idpatients}).then({});
                                    models.HistoryIllness.create({idpatients:result.idpatients}).then({});
                                    models.Medications.create({idpatients:result.idpatients}).then({});
                                    models.ProblemList.create({idpatients:result.idpatients}).then({});
                                    models.PhysicalExam.create({idpatients:result.idpatients}).then({});
                                    models.VisitInformation.create({idpatients:result.idpatients}).then({});
                                }
                                catch($error)
                                {
                                    console.log($error);
                                }
                                res.json( { status_code: '0',message:"Patients inserted",
                                result:result});
                            });
                        }
                    });
  
});

router.post('/update/demographics', auth.authenticate(),function(req, res, next) {
    var demo = models.Demographics;
    var values = req.body;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        res.json( { status_code: '0',message:"Demographics updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                res.json( { status_code: '0',message:"Demographics inserted",
                                result:result});
                            });
                        }
                    });
  
});


function visitInformation(values,callback)
{
    var demo = models.VisitInformation;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}


function chiefComplain(values,callback)
{
    var demo = models.ChiefComplain;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}

function historyIllness(values,callback)
{   var demo = models.HistoryIllness;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"History of present illness updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"History of present illness inserted",
                                result:result});
                            });
                        }
                    });
}

router.post('/update/visitinformation', auth.authenticate(),function(req, res, next) {
    var values = req.body.visit_information;
    visitInformation(values,function(data)
    {
        if(data.status_code=='0')
        {
            values = req.body.chief_complaint;
            chiefComplain(values,function(data)
            {
                if(data.status_code=='0')
                {
                    values = req.body.history_of_present_illness;
                    historyIllness(values,function(data)
                    {
                        if(data.status_code=='0')
                        {
                            res.json( { status_code: '0',message:"records  updated",
                                                });
                        }
                        else
                        {
                            res.json( { status_code: '0',message:data.message});
                        }
                    });
                }
                else
                {
                            res.json( { status_code: '0',message:data.message});
                }
            });
        }
        else
        {
            res.json( { status_code: '0',message:data.message});
        }
    });
  
});

function allergies(values,callback)
{
    var demo = models.Allergies;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}

function medications(values,callback)
{
    var demo = models.Medications;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}

function problemList(values,callback)
{
    var demo = models.ProblemList;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}

function histories(values,callback)
{
    var demo = models.History;
    var objDemo = demo.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            demo.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        callback( { status_code: '0',message:"Chief complain updated",
                                        });
                                    });
                        }
                        else
                        {
                            demo.create(values).then((result)=>{
                                callback( { status_code: '0',message:"Chief complain inserted",
                                result:result});
                            });
                        }
                    });
}

router.post('/update/review', auth.authenticate(),function(req, res, next) {
    var health = models.ReviewSystem;
    var values = req.body;
    var objDemo = health.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            health.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        res.json( { status_code: '0',message:"review  updated",
                                        });
                                    });
                        }
                        else
                        {
                            health.create(values).then((result)=>{
                                res.json( { status_code: '0',message:"review inserted",
                                result:result});
                            });
                        }
                    });
  
});

router.post('/update/health_maintenance',auth.authenticate(), function(req, res, next) {
    var health = models.HealthMaintenance;
    var values = req.body;
    var objDemo = health.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            health.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        res.json( { status_code: '0',message:"health maintenance  updated",
                                        });
                                    });
                        }
                        else
                        {
                            health.create(values).then((result)=>{
                                res.json( { status_code: '0',message:"health maintenance inserted",
                                result:result});
                            });
                        }
                    });
  
 
});

router.post('/update/physical_examination',auth.authenticate(), function(req, res, next) {
    var health = models.PhysicalExam;
    var values = req.body.physical_examination;
    var objDemo = health.findOne({
                        where: {
                            idpatients: values.idpatients
                        }
                    }).then(function(data) {
                        
                        if (data)
                        {
                            health.update(values,{where:{idpatients:values.idpatients}})
                                    .then(result=>{
                                        res.json( { status_code: '0',message:"health maintenance  updated",
                                        });
                                    });
                        }
                        else
                        {
                            health.create(values).then((result)=>{
                                res.json( { status_code: '0',message:"health maintenance inserted",
                                result:result});
                            });
                        }
                    });
  
 
});

router.post('/update/healthstatus',auth.authenticate(), function(req, res, next) {
var values = req.body.allergies;
    allergies(values,function(data)
    {
        if(data.status_code=='0')
        {
            values = req.body.medications;
            medications(values,function(data)
            {
                if(data.status_code=='0')
                {
                    values = req.body.problemlist;
                    problemList(values,function(data)
                    {
                        if(data.status_code=='0')
                        {
                            values = req.body.histories;
                            histories(values,function(data)
                            {
                                if(data.status_code=='0')
                                {
                                    res.json( { status_code: '0',message:"records  updated"});
                                }
                                else
                                {
                                    res.json( { status_code: '0',message:data.message});
                                }
                            });
                        }
                        else
                        {
                            res.json( { status_code: '0',message:data.message});
                        }
                    });
                }
                else
                {
                            res.json( { status_code: '0',message:data.message});
                }
            });
        }
        else
        {
            res.json( { status_code: '0',message:data.message});
        }
    });
  
});

module.exports = router;




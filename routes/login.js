var express = require('express');
var jwt = require('jwt-simple');
var router = express.Router();
var db = require('../models/dbconnection');
var auth = require("../models/authstrategy")();
var cfg = require('../models/config');

router.post("/", function (req, res) {
    if (req.body.username && req.body.password) {
        var username = req.body.username;
        var password = req.body.password;
        if (username && password) {

            var dbcon = db.connect();

            dbcon.getConnection(function (err, con) {

                if (err) {
                    return res.status(401).send('Server status unstable!');
                }

                con.query('SELECT id, name, username, authused FROM users WHERE username="' + username + '" AND password=md5("' + password + '")', function (err, result) {
                    con.release();
                    if (err)
                        return res.status(401).send('Something went wrong!');
                    if (result.length < 1) {
                        return res.status(401).send('Invalid username or password!');
                    }
                    var payload = {
                        username: username,
                        password: password
                    };
                    var token = jwt.encode(payload, cfg.jwtSecret);
                    return res.json({
                        name: result[0].name,
                        username: result[0].username,
                        idusers: result[0].id,
                        token: token,
                        authused: (result[0].authused == 1) ? true : false
                    });

                })
            })
        } else {
            res.sendStatus(401);
        }
    } else {
        res.sendStatus(401);
    }
});

router.post("/authenticate", function (req, res) {
    if (req.body.username && req.body.password) {
        var username = req.body.username;
        var password = req.body.password;
        if (username && password) {

            var dbcon = db.connect();

            dbcon.getConnection(function (err, con) {

                if (err) {
                    return res.status(401).send('Server status unstable!');
                }

                con.query('SELECT id, name,lname,title, username FROM users WHERE username="' + username + '" AND password=md5("' + password + '")', function (err, result) {
                    con.release();
                    if (err)
                        return res.json({status_code:401,message:'Something went wrong!'});
                    if (result.length < 1) {
                        return res.json({status_code:401,message:'Invalid username or password!'});
                    }
                    if (result[0].authused == 1) {
                        if (req.body.code) {

                            var authresult = speakeasy.totp.verify({
                                secret: result[0].secret,
                                encoding: 'base32',
                                token: req.body.code
                            });

                            if (authresult) {
                                var payload = {
                                    username: username,
                                    password: password
                                };
                                var token = jwt.encode(payload, cfg.jwtSecret);
                                return res.json({
                                    name: result[0].name,
                                    username: result[0].username,
                                    idusers: result[0].id,
                                    token: token,
                                    lname:result[0].lname,
                                    title:result[0].title,
                                    authused: (result[0].authused == 1) ? true : false
                                });
                            }
                            else {
                                return res.status(200).send("invalid code");
                            }
                        }
                        else {
                            return res.json({
                                auth2faRequired: true
                            });
                        }
                    }
                    else {
                        var payload = {
                            username: username,
                            password: password
                        };
                        var token = jwt.encode(payload, cfg.jwtSecret);
                        return res.json({
                            status_code:'200',
                            result:{
                                name: result[0].name,
                                username: result[0].username,
                                idusers: result[0].id,
                                token: token,
                                lname:result[0].lname,
                                    title:result[0].title,
                                authused: (result[0].authused == 1) ? true : false
                            }
                        });
                    }

                })
            })
        } else {
            res.sendStatus(401);
        }
    } else {
        res.sendStatus(401);
    }
});

module.exports = router;

-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 13, 2018 at 11:18 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `timiemr`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergies`
--

CREATE TABLE `allergies` (
  `idpatients` int(11) NOT NULL,
  `allergies` longtext,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chief_complaint`
--

CREATE TABLE `chief_complaint` (
  `idpatients` int(11) NOT NULL,
  `complaint` longtext,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `demographics`
--

CREATE TABLE `demographics` (
  `idpatients` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `bmi` decimal(3,3) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `emergencycontact` varchar(20) DEFAULT NULL,
  `homecontact` varchar(20) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `emergency_medicines` varchar(255) DEFAULT NULL,
  `blood_group` varchar(45) DEFAULT NULL,
  `insurence` text NOT NULL,
  `attend_physician` text NOT NULL,
  `consulting_physician` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `iddoctors` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doctors_facilities`
--

CREATE TABLE `doctors_facilities` (
  `iddoctors` int(11) NOT NULL,
  `idfacilities` int(11) NOT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `joindate` timestamp NULL DEFAULT NULL,
  `enddate` timestamp NULL DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doctors_patients`
--

CREATE TABLE `doctors_patients` (
  `iddoctors` int(11) NOT NULL,
  `idpatients` int(11) NOT NULL,
  `startdate` timestamp NULL DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `idfacilities` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `family_history`
--

CREATE TABLE `family_history` (
  `idpatients` int(11) NOT NULL,
  `disease` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `health_maintenance`
--

CREATE TABLE `health_maintenance` (
  `idpatients` int(11) NOT NULL,
  `review_management` longtext,
  `diagnosis` longtext,
  `plan` longtext,
  `patient_instructions` longtext,
  `course` longtext,
  `orders` longtext,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `idpatients` int(11) NOT NULL,
  `past_medical_history` longtext,
  `family_history_disease` varchar(255) DEFAULT NULL,
  `family_history_discription` varchar(255) DEFAULT NULL,
  `procedure_history` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `social_alcohol_assessment` varchar(255) DEFAULT NULL,
  `social_tobacco_assessment` varchar(255) DEFAULT NULL,
  `social_substance_abuse_assessment` varchar(255) DEFAULT NULL,
  `social_employment_and_education_assessment` varchar(255) DEFAULT NULL,
  `social_home_and_environment_assessment` varchar(255) DEFAULT NULL,
  `social_nutrition_and_health_assessment` varchar(255) DEFAULT NULL,
  `social_exercise_and_physical_activity_assessment` varchar(255) DEFAULT NULL,
  `social_sexual_assessment` varchar(255) DEFAULT NULL,
  `social_other_assessment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `history_of_present_illness`
--

CREATE TABLE `history_of_present_illness` (
  `idpatients` int(11) NOT NULL,
  `history` longtext,
  `updatedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medications`
--

CREATE TABLE `medications` (
  `idpatients` int(11) NOT NULL,
  `medications` longtext,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `idpatients` int(11) NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `ethnic_group` varchar(255) DEFAULT NULL,
  `race` varchar(64) DEFAULT NULL,
  `sex` varchar(45) DEFAULT NULL,
  `marital_status` varchar(45) DEFAULT NULL,
  `language` varchar(64) NOT NULL,
  `religion` varchar(64) NOT NULL,
  `address` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patients_facilities`
--

CREATE TABLE `patients_facilities` (
  `idpatients` int(11) NOT NULL,
  `idfacilities` int(11) NOT NULL,
  `grno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `physical_examination`
--

CREATE TABLE `physical_examination` (
  `idpatients` int(11) NOT NULL,
  `vital_signs` datetime NOT NULL,
  `peripheral_pulse_rate` varchar(255) DEFAULT NULL,
  `pulse_site` varchar(255) DEFAULT NULL,
  `hr_method` varchar(255) DEFAULT NULL,
  `systolic_blood_pressure` varchar(255) DEFAULT NULL,
  `diastolic_blood_pressure` varchar(255) DEFAULT NULL,
  `mean_arterial_pressure` varchar(255) DEFAULT NULL,
  `bp_site` varchar(255) DEFAULT NULL,
  `bp_method` varchar(255) DEFAULT NULL,
  `measurements_from_flowsheet` varchar(255) DEFAULT NULL,
  `height_measured` varchar(255) DEFAULT NULL,
  `weight_measured` varchar(255) DEFAULT NULL,
  `bsa` varchar(255) DEFAULT NULL,
  `body_mass_index` varchar(255) DEFAULT NULL,
  `ht_wt_measurement` varchar(255) DEFAULT NULL,
  `general` varchar(255) DEFAULT NULL,
  `eye` varchar(255) DEFAULT NULL,
  `hent` varchar(255) DEFAULT NULL,
  `neck` varchar(255) DEFAULT NULL,
  `respiratory` varchar(255) DEFAULT NULL,
  `cardiovascular` varchar(255) DEFAULT NULL,
  `gastrointestinal` varchar(255) DEFAULT NULL,
  `genitourinary` varchar(255) DEFAULT NULL,
  `lymphatics` varchar(255) DEFAULT NULL,
  `musculoskeletal` varchar(255) DEFAULT NULL,
  `integumentary` varchar(255) DEFAULT NULL,
  `neurologic` varchar(255) DEFAULT NULL,
  `cognition_and_speech` varchar(255) DEFAULT NULL,
  `psychiatric` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `problem_list`
--

CREATE TABLE `problem_list` (
  `idpatients` int(11) NOT NULL,
  `problem_list` longtext,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review_of_systems`
--

CREATE TABLE `review_of_systems` (
  `idpatients` int(11) NOT NULL,
  `constitutional` varchar(255) DEFAULT NULL,
  `eye` varchar(255) DEFAULT NULL,
  `ear_nose_mouth_throat_enmt` varchar(255) DEFAULT NULL,
  `respiratory` varchar(255) DEFAULT NULL,
  `cardiovascular` varchar(255) DEFAULT NULL,
  `gastrointestinal` varchar(255) DEFAULT NULL,
  `genitourinary` varchar(255) DEFAULT NULL,
  `hema_lymph` varchar(255) DEFAULT NULL,
  `endocrine` varchar(255) DEFAULT NULL,
  `immunologic` varchar(255) DEFAULT NULL,
  `musculoskeletal` varchar(255) DEFAULT NULL,
  `integumentary` varchar(255) DEFAULT NULL,
  `neurologic` varchar(255) DEFAULT NULL,
  `psychiatric` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scanned_documents`
--

CREATE TABLE `scanned_documents` (
  `idpatients` int(11) NOT NULL,
  `document_path` varchar(45) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_added_medicines`
--

CREATE TABLE `user_added_medicines` (
  `idpatients` int(11) NOT NULL,
  `medicine_name` varchar(255) DEFAULT NULL,
  `power` varchar(255) DEFAULT NULL,
  `major_content` varchar(255) DEFAULT NULL,
  `dosage` varchar(255) DEFAULT NULL,
  `mfg_date` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `visit_information`
--

CREATE TABLE `visit_information` (
  `idpatients` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `date_of_service` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `performing_location` varchar(255) DEFAULT NULL,
  `primary_care_provider` varchar(255) DEFAULT NULL,
  `referring_provider` varchar(255) DEFAULT NULL,
  `visit_type` varchar(255) DEFAULT NULL,
  `accompanied_by` varchar(255) DEFAULT NULL,
  `source_of_history` varchar(255) DEFAULT NULL,
  `referral_source` varchar(255) DEFAULT NULL,
  `updatedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allergies`
--
ALTER TABLE `allergies`
  ADD KEY `idpatients` (`idpatients`);

--
-- Indexes for table `chief_complaint`
--
ALTER TABLE `chief_complaint`
  ADD KEY `patient_idx` (`idpatients`);

--
-- Indexes for table `demographics`
--
ALTER TABLE `demographics`
  ADD KEY `demographics_of_idx` (`idpatients`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`iddoctors`);

--
-- Indexes for table `doctors_facilities`
--
ALTER TABLE `doctors_facilities`
  ADD KEY `service_provider_idx` (`iddoctors`),
  ADD KEY `service_location_idx` (`idfacilities`);

--
-- Indexes for table `doctors_patients`
--
ALTER TABLE `doctors_patients`
  ADD KEY `consultant_idx` (`iddoctors`),
  ADD KEY `consulting_idx` (`idpatients`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`idfacilities`);

--
-- Indexes for table `family_history`
--
ALTER TABLE `family_history`
  ADD KEY `fk_family_history_patients1_idx` (`idpatients`);

--
-- Indexes for table `health_maintenance`
--
ALTER TABLE `health_maintenance`
  ADD KEY `patients_idx` (`idpatients`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD KEY `patients_idx` (`idpatients`);

--
-- Indexes for table `history_of_present_illness`
--
ALTER TABLE `history_of_present_illness`
  ADD KEY `idpatients` (`idpatients`);

--
-- Indexes for table `medications`
--
ALTER TABLE `medications`
  ADD KEY `idpatients` (`idpatients`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`idpatients`);

--
-- Indexes for table `patients_facilities`
--
ALTER TABLE `patients_facilities`
  ADD KEY `patient_idx` (`idpatients`),
  ADD KEY `facility_idx` (`idfacilities`);

--
-- Indexes for table `physical_examination`
--
ALTER TABLE `physical_examination`
  ADD KEY `patient_idx` (`idpatients`);

--
-- Indexes for table `problem_list`
--
ALTER TABLE `problem_list`
  ADD KEY `idpatients` (`idpatients`);

--
-- Indexes for table `review_of_systems`
--
ALTER TABLE `review_of_systems`
  ADD KEY `patients_idx` (`idpatients`);

--
-- Indexes for table `scanned_documents`
--
ALTER TABLE `scanned_documents`
  ADD KEY `fk_scanned_documents_patients_idx` (`idpatients`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_added_medicines`
--
ALTER TABLE `user_added_medicines`
  ADD KEY `fk_user_added_medicines_patients1_idx` (`idpatients`);

--
-- Indexes for table `visit_information`
--
ALTER TABLE `visit_information`
  ADD KEY `patients_idx` (`idpatients`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `iddoctors` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `idfacilities` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `idpatients` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `allergies`
--
ALTER TABLE `allergies`
  ADD CONSTRAINT `allergies_ibfk_1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`);

--
-- Constraints for table `chief_complaint`
--
ALTER TABLE `chief_complaint`
  ADD CONSTRAINT `patient_complaint` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `demographics`
--
ALTER TABLE `demographics`
  ADD CONSTRAINT `demographics_of` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctors_facilities`
--
ALTER TABLE `doctors_facilities`
  ADD CONSTRAINT `service_location` FOREIGN KEY (`idfacilities`) REFERENCES `facilities` (`idfacilities`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `service_provider` FOREIGN KEY (`iddoctors`) REFERENCES `doctors` (`iddoctors`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doctors_patients`
--
ALTER TABLE `doctors_patients`
  ADD CONSTRAINT `consultant` FOREIGN KEY (`iddoctors`) REFERENCES `doctors` (`iddoctors`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `consulting` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `family_history`
--
ALTER TABLE `family_history`
  ADD CONSTRAINT `fk_family_history_patients1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `health_maintenance`
--
ALTER TABLE `health_maintenance`
  ADD CONSTRAINT `patients` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `histories`
--
ALTER TABLE `histories`
  ADD CONSTRAINT `patients_histories` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_of_present_illness`
--
ALTER TABLE `history_of_present_illness`
  ADD CONSTRAINT `history_of_present_illness_ibfk_1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`);

--
-- Constraints for table `medications`
--
ALTER TABLE `medications`
  ADD CONSTRAINT `medications_ibfk_1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`);

--
-- Constraints for table `patients_facilities`
--
ALTER TABLE `patients_facilities`
  ADD CONSTRAINT `facility` FOREIGN KEY (`idfacilities`) REFERENCES `facilities` (`idfacilities`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `patient_hospital` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `physical_examination`
--
ALTER TABLE `physical_examination`
  ADD CONSTRAINT `patient` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `problem_list`
--
ALTER TABLE `problem_list`
  ADD CONSTRAINT `problem_list_ibfk_1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`);

--
-- Constraints for table `review_of_systems`
--
ALTER TABLE `review_of_systems`
  ADD CONSTRAINT `patients_systems` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `scanned_documents`
--
ALTER TABLE `scanned_documents`
  ADD CONSTRAINT `fk_scanned_documents_patients` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_added_medicines`
--
ALTER TABLE `user_added_medicines`
  ADD CONSTRAINT `fk_user_added_medicines_patients1` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `visit_information`
--
ALTER TABLE `visit_information`
  ADD CONSTRAINT `patients_information` FOREIGN KEY (`idpatients`) REFERENCES `patients` (`idpatients`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('histories', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
        past_medical_history: {
            type: DataTypes.TEXT
        },
        family_history_disease: {
            type: DataTypes.STRING
        },
        family_history_discription: {
            type: DataTypes.STRING
        },
        procedure_history: {
            type: DataTypes.STRING
        },
       
        social_alcohol_assessment: {
            type: DataTypes.STRING
        },
        social_tobacco_assessment: {
            type: DataTypes.STRING
        },
        social_substance_abuse_assessment: {
            type: DataTypes.STRING
        },
        social_employment_and_education_assessment: {
            type: DataTypes.STRING
        },
        social_home_and_environment_assessment: {
            type: DataTypes.STRING
        },
        social_nutrition_and_health_assessment: {
            type: DataTypes.STRING
        },
        social_exercise_and_physical_activity_assessment: {
            type: DataTypes.STRING
        },
        social_sexual_assessment: {
            type: DataTypes.STRING
        },
        social_other_assessment: {
            type: DataTypes.STRING
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};


module.exports = function(sequelize, DataTypes) {
  return sequelize.define('review_of_systems', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
        constitutional: {
            type: DataTypes.TEXT
        },
        eye: {
            type: DataTypes.TEXT
        }
        ,ear_nose_mouth_throat_enmt: {
            type: DataTypes.TEXT
        },
        respiratory: {
            type: DataTypes.TEXT
        },
        cardiovascular: {
            type: DataTypes.TEXT
        },
        gastrointestinal: {
            type: DataTypes.TEXT
        },
        genitourinary: {
            type: DataTypes.TEXT
        },
        hema_lymph: {
            type: DataTypes.TEXT
        },
        endocrine: {
            type: DataTypes.TEXT
        },
        immunologic: {
            type: DataTypes.TEXT
        },
        musculoskeletal: {
            type: DataTypes.TEXT
        },
        integumentary: {
            type: DataTypes.TEXT
        },
        neurologic: {
            type: DataTypes.TEXT
        },
        psychiatric: {
            type: DataTypes.TEXT
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};


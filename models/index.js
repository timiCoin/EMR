var Sequelize = require('sequelize');
var config    = {database:"timiEMR",username:"root",password:"SuperBull1"};

// initialize database connection
var sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  {
        host: "127.0.0.1",
        dialect: 'mysql',
        define: {
            timestamps: false
        }
    }
);

// load models
var models = [
  'Patient',
  'Demographics',
  'HealthMaintenance',
  'VisitInformation',
  'ChiefComplain',
  'HistoryIllness',
  'ReviewSystem',
  'ProblemList',
  'Allergies',
  'Medications',
  'History',
  'PhysicalExam',
  'User'
];
models.forEach(function(model) {
  module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// describe relationships
(function(m) {

  /*m.PhoneNumber.belongsTo(m.User);
  m.Task.belongsTo(m.User);
  m.User.hasMany(m.Task);
  m.User.hasMany(m.PhoneNumber);*/
})(module.exports);

// export connection
module.exports.sequelize = sequelize;

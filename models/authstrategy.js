'use strict';

var passport = require("passport");
var passportJWT = require("passport-jwt");
var cfg = require("./config");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var db = require('../models/dbconnection');

var params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt')
};

module.exports = function() {
    var strategy = new Strategy(params, function(payload, done) {

        var dbcon = db.connect();

        dbcon.getConnection(function (err, con) {

            if(err) {
                console.log(err);
                return false;
            }

            con.query('SELECT id, name, username FROM users WHERE username="' + payload.username + '" AND password=md5("' + payload.password + '")', function (err, result) {
                con.release();
                if(err)
                    return done(new Error("Connection Error"), null);
                if(result.length < 1)
                    return done(new Error("User not found"), null);
                return done(null, {
                    username: payload.username,
                    password: payload.password,
                    userid: result[0].id
                });

            })
        })
    });
    passport.use(strategy);
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};

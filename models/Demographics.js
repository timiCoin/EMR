module.exports = function(sequelize, DataTypes) {
  return sequelize.define('demographics', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
 
        weight: {
            type: DataTypes.INTEGER
        },
 
        height: {
            type: DataTypes.INTEGER
        },
 
        bmi: {
            type: DataTypes.NUMERIC
        },
 
        contact: {
            type: DataTypes.STRING
        },
 
        emergencycontact: {
            type: DataTypes.STRING,
            
        },
        emergency_name: {
            type: DataTypes.STRING
        },
 
        emergency_medicines: {
            type: DataTypes.STRING
        },
 
        blood_group: {
            type: DataTypes.STRING
        },
        insurence: {
            type: DataTypes.STRING
        },
 
        attend_physician: {
            type: DataTypes.STRING
        },
 
        consulting_physician: {
            type: DataTypes.STRING
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};


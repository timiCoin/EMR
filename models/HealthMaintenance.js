module.exports = function(sequelize, DataTypes) {
  return sequelize.define('health_maintenance', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
 
        review_management: {
            type: DataTypes.TEXT
        },
 
        diagnosis: {
            type: DataTypes.TEXT
        },
 
        plan: {
            type: DataTypes.TEXT
        },
 
        patient_instructions: {
            type: DataTypes.TEXT
        },
 
        course: {
            type: DataTypes.TEXT,
            
        },
        orders: {
            type: DataTypes.TEXT
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};


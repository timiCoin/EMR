module.exports = function(sequelize, DataTypes) {
  return sequelize.define('physical_examination', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
        peripheral_pulse_rate: {
            type: DataTypes.TEXT
        },
        vital_signs: {
            type: DataTypes.STRING
        },
        vital_time:{
            type: DataTypes.STRING
        },
        pulse_site: {
            type: DataTypes.TEXT
        }
        ,hr_method: {
            type: DataTypes.TEXT
        },
        systolic_blood_pressure: {
            type: DataTypes.TEXT
        },
        diastolic_blood_pressure: {
            type: DataTypes.TEXT
        },
        mean_arterial_pressure: {
            type: DataTypes.TEXT
        },
        bp_site: {
            type: DataTypes.TEXT
        },
        bp_method: {
            type: DataTypes.TEXT
        },
        measurements_from_flowsheet: {
            type: DataTypes.TEXT
        },
        height_measured: {
            type: DataTypes.TEXT
        },
        weight_measured: {
            type: DataTypes.TEXT
        },
        bsa: {
            type: DataTypes.TEXT
        },
        body_mass_index: {
            type: DataTypes.TEXT
        },
        ht_wt_measurement: {
            type: DataTypes.TEXT
        },
        general: {
            type: DataTypes.TEXT
        },
        eye: {
            type: DataTypes.TEXT
        }
        ,hent: {
            type: DataTypes.TEXT
        },
        neck: {
            type: DataTypes.TEXT
        },
        respiratory: {
            type: DataTypes.TEXT
        },
        cardiovascular: {
            type: DataTypes.TEXT
        },
        gastrointestinal: {
            type: DataTypes.TEXT
        },
        genitourinary: {
            type: DataTypes.TEXT
        },
        lymphatics: {
            type: DataTypes.TEXT
        },
        musculoskeletal: {
            type: DataTypes.TEXT
        },
        integumentary: {
            type: DataTypes.TEXT
        },
        neurologic: {
            type: DataTypes.TEXT
        },
        cognition_and_speech: {
            type: DataTypes.TEXT
        },
        psychiatric: {
            type: DataTypes.TEXT
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};


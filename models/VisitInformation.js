module.exports = function(sequelize, DataTypes) {
  return sequelize.define('visit_information', {
        idpatients: {
            unique:true,
            primaryKey:true,
            type: DataTypes.INTEGER
        },
 
        date_of_service: {
            type:DataTypes.STRING
        },
        service_time:{
            type:DataTypes.STRING
        },
        author:{
            type:DataTypes.STRING
        },
        performing_location: {
            type: DataTypes.TEXT
        },
 
        referring_provider: {
            type: DataTypes.TEXT
        },
 
        visit_type: {
            type: DataTypes.TEXT
        },
 
        accompanied_by: {
            type: DataTypes.TEXT,
            
        },
        referral_source: {
            type: DataTypes.TEXT
        },
        primary_care_provider:{
            type:DataTypes.STRING
        }
        ,
        source_of_history:{
            type:DataTypes.STRING
        }
  },{
  // don't add the timestamp attributes (updatedAt, createdAt)
  timestamps: false,

  // don't delete database entries but set the newly added attribute deletedAt
  // to the current date (when deletion was done). paranoid will only work if
  // timestamps are enabled
  paranoid: true,

  // don't use camelcase for automatically added attributes but underscore style
  // so updatedAt will be updated_at
  underscored: true,

  // disable the modification of tablenames; By default, sequelize will automatically
  // transform all passed model names (first parameter of define) into plural.
  // if you don't want that, set the following
  freezeTableName: true
});
};

